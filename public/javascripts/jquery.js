$(document).ready(function(){
  
  $('#form-message').submit(function(){
    socket.emit('chat message', $('#message').val());
    $('#message').val('');
    return false;
  });
  
  $('#btn-join').click(function(){
    socket.emit('join');
  });
  
  socket.on('chat message', function(msg) {
    $('#messages').append($('<li>').text(msg));
  });
});