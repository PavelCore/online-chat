var express = require('express');

function initSocket(server) {
  var io = require('socket.io')(server);
  var roomName = "default Name";
  
  io.on('connection', function(socket){
    var myId = socket.id;
    console.log('a user connected');
  
    socket.on('disconnect', function(){
      console.log('user disconnected');
    });
    
    socket.on('chat message', function(msg) {
      console.log('message:' + msg);
      
      io.sockets.in(roomName).emit('chat message', msg);
    });
    
    socket.on('join', function() {
      socket.join(roomName);
    });
  });
}

module.exports = initSocket;